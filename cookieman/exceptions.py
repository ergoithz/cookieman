"""Cookieman exceptions."""


class CookieSizeException(Exception):
    """Cookie exceeding maximum size exception."""
